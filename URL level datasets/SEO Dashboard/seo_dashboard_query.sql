WITH

tags_by_browser_url AS (
  SELECT DISTINCT
    name
  , id
  , type
  , browser_url
  FROM `qdoo-bi.dimensions.search_tags`
),

web_sessions_url AS (
    SELECT 
    CONCAT(domain, landing_page_short) as url 
    , bounce
    , sid
    , date
    , CASE WHEN session_success = true THEN 1 ELSE 0 END AS trx
    FROM `qdoo-bi.facts.web_sessions` 
    WHERE date >= '2020-01-01'
    AND channel = 'SEO'
),

cities_and_districts AS (
    SELECT 
    sid
    , MAX(city) AS city 
    , MAX(district) AS district
    FROM `ft-explore.product.listings_performance`
    GROUP BY 1 
),

sessions_and_bounce_data AS (
    SELECT 
    date
    , URL
    , COUNT(URL) AS number_of_sessions
    , SUM(CASE bounce WHEN true THEN 1 ELSE 0 END) as number_of_bounces
    , MAX(cities_and_districts.city) AS city
    , MAX(cities_and_districts.district) AS district
    , SUM(trx) AS trx
    FROM web_sessions_url  
    LEFT JOIN cities_and_districts ON web_sessions_url.sid = cities_and_districts.sid
    GROUP BY 1, 2
),

search_console_data AS (
    SELECT
    NET.HOST(keys) as domain # selecting domain from pages supplied by google_search_console
    , REPLACE(REPLACE(keys,"['",""),"']","") as full_page #selecting full page domain + page_path from pages supplied by google search_console
    , clicks
    , ctr
    , impressions
    , position
    , date
    , country
    FROM `qdoo-data.marketing.google_searchconsole`

    WHERE
        keys NOT LIKE '%/place/%' #excluding RDP pages
    # excluding other language pages
    AND keys NOT LIKE '%/en/%'
    AND keys NOT LIKE '%/de/%'
    AND keys NOT LIKE '%/fi/%'
    AND keys NOT LIKE '%/fr/%'
    AND keys NOT LIKE '%/it/%'
    AND keys NOT LIKE '%/nl/%'
    AND keys NOT LIKE '%/tr/%'
    AND keys NOT LIKE '%/hk/%'
    AND keys NOT LIKE '%/pl/%'
    #I am filtering countries here as we take specific cities and districts only - it ensures valid data
    AND country IN ("DE","SG","GB","AU","IT","AT","FI","NL","HK")
),

udf_functions AS (
    SELECT
     `qdoo-bi.udf.getCountryFromDomain`(domain) AS domain_country #selecting country based on domain
    , IF(REGEXP_CONTAINS(full_page
        , 'near-me|in-der-naehe|laehellae|a-proximite|vicino-a-me|in-de-buurt|yakinlardaki')
      , TRUE, FALSE) AS nearme_flag #flag used later for listing_composition
    , CASE WHEN full_page like '%?%' THEN split(full_page,'?')[OFFSET(0)] ELSE full_page END as pagePath_short
    , CASE WHEN full_page like '%poi%' THEN TRUE ELSE FALSE END as poi_flag #flag used later for listing_composition
    , CASE WHEN full_page like '%/tags/%' THEN TRUE ELSE FALSE END as tag_flag #flag used later for listing_composition
    , `qdoo-bi.udf.getTagIdfromPagePath`(full_page) AS tags_browser_url
    , REPLACE(full_page,"https://","") as full_page
    , domain
    , clicks
    , ctr
    , position
    , impressions
    , date 
    FROM search_console_data
),

    final AS (SELECT

    #listing_composition
        CASE
        WHEN nearme_flag
        AND NOT tag_flag
        AND (t.name IS NULL
            OR t.name = 'Eat & Drink')
        THEN 'NEAR_ME'
        WHEN nearme_flag
            AND tag_flag
            AND (t.type != 'CUISINE'
                OR t.type  IS NULL)
        THEN 'NEAR_ME+TAG'
        WHEN nearme_flag
            AND ((t.type = 'CUISINE'
                AND (t.name  != 'Eat & Drink'
                    OR t.name IS NULL))
            OR (t.name IS NOT NULL
                AND t.name != 'Eat & Drink'))
        THEN 'NEAR_ME+CUISINE'
        WHEN poi_flag
            AND NOT tag_flag
            AND (t.name IS NULL
            OR t.name = 'Eat & Drink')
        THEN 'POI'
        WHEN poi_flag
            AND tag_flag
            AND (t.type != 'CUISINE'
                OR t.type IS NULL)
        THEN 'POI+TAG'
        WHEN poi_flag
            AND ((t.type = 'CUISINE'
                AND (t.name != 'Eat & Drink'
                    OR t.name IS NULL))
            OR (t.name IS NOT NULL
                AND t.name  != 'Eat & Drink'))
        THEN 'POI+CUISINE'
        WHEN city IS NOT NULL
            AND district IS NULL
            AND tag_flag
            AND (t.type != 'CUISINE'
                OR t.type IS NULL)
        THEN 'CITY+TAG'
        WHEN city IS NOT NULL
            AND district IS NULL
            AND ((t.type = 'CUISINE'
                AND (t.name != 'Eat & Drink'
                    OR t.name IS NULL))
            OR (t.name IS NOT NULL
                AND t.name != 'Eat & Drink'))
        THEN 'CITY+CUISINE'
        WHEN city IS NOT NULL
            AND district IS NULL
            AND (t.type IS NULL
                OR t.name = 'Eat & Drink')
        THEN 'ONLY CITY'
        WHEN city IS NOT NULL
            AND district IS NOT NULL
            AND tag_flag
            AND (t.type != 'CUISINE'
                OR t.type IS NULL )
        THEN 'CITY+DISTRICT+TAG'
        WHEN city IS NOT NULL
            AND district IS NOT NULL
            AND ((t.type = 'CUISINE'
                AND (t.name != 'Eat & Drink'
                    OR t.name IS NULL))
            OR (t.name IS NOT NULL
                AND t.name  != 'Eat & Drink'))
        THEN 'CITY+DISTRICT+CUISINE'
        WHEN city IS NOT NULL
            AND district IS NOT NULL
            AND (t.type IS NULL
                OR t.name  = 'Eat & Drink')
        THEN 'CITY+DISTRICT'
        ELSE 'OTHER'
        END AS listingcomposition
    , CASE WHEN full_page LIKE '%/place/%' THEN 1 ELSE 0 END AS is_rdp
    , u.clicks
    , u.ctr
    , u.position
    , u.date
    , u.impressions
    , u.domain
    , u.full_page
    , u.domain_country
    , s.district
    , s.city
    , s.number_of_bounces
    , s.number_of_sessions
    , s.trx

    FROM udf_functions u
    LEFT JOIN tags_by_browser_url t ON u.tags_browser_url = t.browser_url
    RIGHT JOIN sessions_and_bounce_data s ON (s.URL = u.full_page AND s.date = u.date)
    WHERE
         city IN ('Sydney'
                    ,'Melbourne'
                    ,'Brisbane'
                    ,'London'
                    ,'Edinburgh'
                    ,'Berlin'
                    ,'München'
                    ,'Wien'
                    ,'Helsinki'
                    ,'Amsterdam'
                    ,'Singapore'
                    ,'Hong Kong'
                    ,'Roma'
                    ,'Milano')
        AND u.date > '2020-01-01')

SELECT 
*
FROM final
